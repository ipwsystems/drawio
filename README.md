ipwsystems/drawio
-----
This a composer mirror of *jgraph/drawio* to be used in IPW Metazo.

For more details see https://github.com/jgraph/drawio.